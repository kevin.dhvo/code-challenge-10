package com.example.codechallenge10

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.codechallenge10.ui.theme.CodeChallenge10Theme
import com.example.codechallenge10.ui.theme.FunkyShape

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CodeChallenge10Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Content()
                }
            }
        }
    }
}

@Composable
fun Content() {
    BoxWithConstraints(Modifier
        .fillMaxSize()
    ) {
        BoxWithConstraints(modifier = Modifier
            .padding(start = 25.dp, top = 50.dp)
            .fillMaxSize()
            .background(
                Brush.verticalGradient(
                    colors = listOf(MaterialTheme.colors.secondary, MaterialTheme.colors.primary),
                    startY = (with(LocalDensity.current) { maxHeight.toPx() }) * 0.3f,
                ),
                FunkyShape(with(LocalDensity.current) { 75.dp.toPx() })
            ),
            contentAlignment = Alignment.BottomStart
        ) {
            Row(
                modifier = Modifier
                    .padding(start = 30.dp, bottom = maxHeight*(2f/25f))
                    .size(
                        width = (maxWidth.div(2.25.dp)).dp,
                        height = 75.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "Get Started",
                    style = MaterialTheme.typography.body1.copy(
                        fontWeight = FontWeight.ExtraBold,
                        fontSize = 18.sp
                    )
                )
                Icon(Icons.Default.ArrowForward, contentDescription = null)
            }

        }
    }
}


// Ignore these previews...they are wrong
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CodeChallenge10Theme {
        Content()
    }
}

@Preview(showBackground = true)
@Composable
fun DarkDefaultPreview() {
    CodeChallenge10Theme(darkTheme = true) {
        Content()
    }
}