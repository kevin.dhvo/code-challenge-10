package com.example.codechallenge10.ui.theme

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(4.dp),
    large = RoundedCornerShape(0.dp)
)

class FunkyShape(private val cornerRadius: Float) : Shape {

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        return Outline.Generic(
            path = drawPath(size = size, cornerRadius = cornerRadius)
        )
    }
}

fun drawPath(size: Size, cornerRadius: Float): Path {
    return Path().apply {
        reset()
        arcTo(
            rect = Rect(
                left = 0f,
                top = 0f,
                right = cornerRadius,
                bottom = cornerRadius
            ),
            startAngleDegrees = 180.0f,
            sweepAngleDegrees = 90.0f,
            forceMoveTo = false
        )

        lineTo(x = size.width, y = 0f)
        lineTo(x = size.width, y = size.height)
        lineTo(x = size.width*(1f/6f), y = size.height)
        lineTo(x = size.width*(1f/6f), y = size.height*(23f/25f))

        arcTo(
            rect = Rect(
                left = size.width*(1f/6f),
                top = size.height*(23f/25f),
                right = size.width*(1f/6f) + cornerRadius,
                bottom = size.height*(23f/25f) + cornerRadius
            ),
            startAngleDegrees = 180.0f,
            sweepAngleDegrees = 90.0f,
            forceMoveTo = false
        )
        lineTo(x = size.width/2f, y = size.height*(23f/25f))

        arcTo(
            rect = Rect(
                left = size.width/2.5f,
                top = size.height*(23f/25f) - cornerRadius,
                right = size.width/2.5f + cornerRadius,
                bottom = size.height*(23f/25f)
            ),
            startAngleDegrees = 90.0f,
            sweepAngleDegrees = -180.0f,
            forceMoveTo = false
        )
        lineTo(x = 0f, y = size.height*(23f/25f) - cornerRadius)

        arcTo(
            rect = Rect(
                left = 0f,
                top = size.height*(23f/25f) - 2*cornerRadius,
                right = cornerRadius,
                bottom = size.height*(23f/25f) - cornerRadius
            ),
            startAngleDegrees = 90.0f,
            sweepAngleDegrees = 90.0f,
            forceMoveTo = false
        )
        close()
    }
}

@Preview()
@Composable
private fun FunkyShapePreview() {
    CodeChallenge10Theme() {
        Surface(
            modifier = Modifier.fillMaxSize(),
            shape = FunkyShape(5f)
        ) {
        }
    }
}