package com.example.codechallenge10.ui.theme

import androidx.compose.ui.graphics.Color

val Magentaish = Color(0xffac1c4d)
val Purpleish = Color(0xff7e1b4e)
val Orange = Color(0xFFF85B14)
